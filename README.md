# makru-langc-new
A mini program to create directory layout for c project using makru_langc

## Usage

Recommended way to install makru-langc-new by pip:

````
pip install makru-langc-new
````

Commandline usage:
````
makru_langc_new  [-Nname] [-Vversion] [-Ttype] [-h/--help]
````

Any of options can be simply ignore, then makru_langc_new will ask you in interactive way.

## LICENSE

    makru_langc_new
    Copyright (C) 2021  Jin Wa, and the project contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
     any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
